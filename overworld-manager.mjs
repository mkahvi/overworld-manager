import { CFG } from './module/config.mjs';
import { OverworldOverview } from './module/actor-monitor.mjs';
import { AgentSheet, MonsterSheet, SquadSheet } from './module/actor/_sheets.mjs';

Hooks.on('renderActorDirectory', function (dir, jq, options) {
	const html = jq[0];

	const foot = html.querySelector('.directory-footer');

	const button = document.createElement('button');
	button.classList.add('overworld-manager-button');
	button.innerText = 'Overworld Overview';
	button.addEventListener('click', ev => {
		ev.preventDefault();
		ev.stopPropagation();

		new OverworldOverview().render(true, { focus: true });
	});

	foot.append(button);
});

const debounceAMrerender = foundry.utils.debounce(() => Object.values(ui.windows).find(app => app instanceof OverworldOverview)?.render(), 150);

Hooks.on('updateActor', () => {
	debounceAMrerender();
});

Hooks.once('init', () => {
	game.settings.register(CFG.module, 'actors', {
		type: Object,
		default: {},
		scope: 'world',
		config: false,
		onChange: _ => debounceAMrerender()
	});
});

Hooks.once('init', () => {
	console.log('Overworld Manager | Initializing');

	console.log('Overworld Manager | Finalizing');

	const npcTypes = ['npc'],
		pcTypes = ['character'],
		nonPrimary = ['npc', 'basic'],
		allTypes = [...npcTypes, ...pcTypes];

	Actors.registerSheet('overworld-manager', AgentSheet, { label: 'Overworld.Sheet.AgentSheet', types: nonPrimary, makeDefault: false });
	Actors.registerSheet('overworld-manager', MonsterSheet, { label: 'Overworld.Sheet.MonsterSheet', types: nonPrimary, makeDefault: false });
	Actors.registerSheet('overworld-manager', SquadSheet, { label: 'Overworld.Sheet.SquadSheet', types: nonPrimary, makeDefault: false });
});
