import { CFG } from '../config.mjs';

export class MonsterSheet extends ActorSheet {
	get template() {
		return `modules/${CFG.module}/template/actor/monster.hbs`;
	}

	static get defaultOptions() {
		const _default = super.defaultOptions;

		return {
			..._default,
			classes: ['sheet', 'overworld-manager', 'actor', 'monster'],
			width: 'auto',
			height: 'auto',
			resizable: false,
		};
	}

	getData() {
		const _default = super.getData();

		const systemData = mergeObject({
			actions: {
				attack: false,
				move: false,
			},
			skills: {
				fight: {
					total: 0,
					bonus: 0,
				},
			},
			dc: {
				total: 0,
				bonus: 0,
			},
			size: 0,
		}, this.document.data.data.overworld);

		const strength = systemData.size;

		systemData.skills.fight.total = strength + systemData.skills.fight.bonus;
		systemData.dc.total = 10 + strength + systemData.dc.bonus;

		return {
			..._default,
			actor: this.document,
			data: systemData,
			actions: systemData.actions,
			skills: systemData.skills,
			dc: systemData.dc,
			notes: systemData.notes,
		};
	}

	/**
	 * @param {Event} ev
	 */
	_activate(ev, { action } = {}) {
		ev?.preventDefault();
		ev?.stopPropagation();

		const consumeMove = action === 'move';

		const el = ev?.currentTarget;
		action = el?.dataset.action ?? action;

		switch (action) {
			case 'move': {
				if (consumeMove) this.document.update({ 'data.overworld.actions.move': true });
				new Roll('1d6').evaluate({ async: true })
					.then(r => r.toMessage({ flavor: 'Movement' }, { rollMode: 'selfroll' }));
				break;
			}
		}
	}

	/**
	 * @param {Event} ev
	 */
	async _roll(ev) {
		ev?.preventDefault();
		ev?.stopPropagation();

		const el = ev.currentTarget;
		const skill = el.dataset.skill;
		const systemData = this.document.data.data.overworld;

		// Initialize
		const skData = systemData.skills[skill] ?? {};
		['bonus']
			.forEach(key => {
				if (!Number.isSafeInteger(skData[key])) skData[key] = 0;
			});

		// Consume actions
		const act = systemData.actions;
		let usedAction = 0;
		if (!ev.shiftKey) {
			if (act.attack !== true) {
				this.document.update({ 'data.overworld.actions.attack': true });
				usedAction = 1;
			}
		}
		else
			usedAction = -1;

		const actionMsg = usedAction < 0 ? ' (Reaction)' : usedAction == 0 ? ' (No Actions Left)' : '';

		// Build a roll
		const parts = ['1d20', `${systemData.size}[Strength]`];
		if (skData.bonus != 0) parts.push(`${skData.bonus}[Bonus]`);

		const roll = await new Roll(parts.join(' + ')).evaluate({ async: true });

		const chatData = {
			speaker: { actor: this.document },
			flavor: game.i18n.localize(`Overworld.Skill.${skill}`) + actionMsg,
		};
		roll.toMessage(chatData, { rollMode: 'selfroll' });
	}

	_resetActions(ev) {
		ev?.preventDefault();
		ev?.stopPropagation();

		const actionUpdate = {
			'data.overworld.actions.move': false,
			'data.overworld.actions.attack': false,
		};

		this.document.update(actionUpdate);
	}

	activateListeners(jq) {
		super.activateListeners(jq);
		const html = jq[0];

		html.querySelectorAll('.rollable').forEach(el => el.addEventListener('click', this._roll.bind(this)));

		html.querySelectorAll('.activatable').forEach(el => el.addEventListener('click', this._activate.bind(this)));

		html.querySelectorAll('input.move').forEach(el => el.addEventListener('click', ev => {
			if (ev.target.checked)
				this._activate(null, { action: 'move' });
		}));

		html.querySelector('button.reset')?.addEventListener('click', this._resetActions.bind(this));
	}

	async _updateObject(event, formData) {
		const name = formData['name'];

		// Update prototype
		let nameChanged = false;
		if (name !== this.document.data.token.name) {
			formData['token.name'] = name;
			nameChanged = true;
		}

		await super._updateObject(event, formData);
	}
}
