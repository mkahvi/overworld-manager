import { CFG } from '../config.mjs';

export class SquadSheet extends ActorSheet {
	get template() {
		return `modules/${CFG.module}/template/actor/squad.hbs`;
	}

	static get defaultOptions() {
		const _default = super.defaultOptions;

		return {
			..._default,
			classes: ['sheet', 'overworld-manager', 'actor', 'squad'],
			width: 'auto',
			height: 'auto',
			resizable: false,
		};
	}

	getData() {
		const _default = super.getData();

		const systemData = mergeObject({
			actions: {
				act1: false,
				act2: false,
				act3: false,
				camp: false,
			},
			skills: {
				act: {
					total: 0,
				},
			},
			dc: {
				total: 0,
				bonus: 0,
			},
			size: 0,
			wounded: 0,
		}, this.document.data.data.overworld);

		const strength = systemData.size;

		const sk = systemData.skills.act;
		systemData.skills.act.total = strength + (sk.base ?? 0) + (sk.eq ?? 0) + (sk.res ?? 0);
		systemData.dc.total = 10 + strength + systemData.dc.bonus;

		return {
			..._default,
			actor: this.document,
			data: systemData,
			actions: systemData.actions,
			skills: systemData.skills,
			campReady: systemData.actions.act1 && systemData.actions.act2 && systemData.actions.act3 && !systemData.actions.camp,
			dc: systemData.dc,
			notes: systemData.notes,
		};
	}

	/**
	 * @param {Event} ev
	 */
	async _roll(ev) {
		ev?.preventDefault();
		ev?.stopPropagation();

		const el = ev.currentTarget;
		const skill = el.dataset.skill;
		const systemData = this.document.data.data.overworld;

		// Initialize
		const skData = systemData.skills[skill] ?? {};
		['total', 'base', 'eq', 'res']
			.forEach(key => {
				if (!Number.isSafeInteger(skData[key])) skData[key] = 0;
			});

		// Consume actions
		const act = systemData.actions;
		let usedAction = 0;
		if (!ev.shiftKey) {
			if (act.act1 !== true) {
				this.document.update({ 'data.overworld.actions.act1': true });
				usedAction = 1;
			}
			else if (act.act2 !== true) {
				this.document.update({ 'data.overworld.actions.act2': true });
				usedAction = 2;
			}
			else if (act.act3 !== true) {
				this.document.update({ 'data.overworld.actions.act3': true });
				usedAction = 3;
			}
		}
		else
			usedAction = -1;

		const actionMsg = usedAction < 0 ? ' (Reaction)' : usedAction == 0 ? ' (No Actions Left)' : ` (Action ${usedAction})`;

		// Build a roll
		const parts = ['1d20', `${systemData.size}[Strength]`];
		if (skData.eq != 0) parts.push(`${skData.eq}[Equipment]`);
		if (skData.res != 0) parts.push(`${skData.res}[Research]`);

		const roll = await new Roll(parts.join(' + ')).evaluate({ async: true });

		roll.toMessage({
			speaker: { actor: this.document },
			flavor: game.i18n.localize(`Overworld.Skill.${skill}`) + actionMsg,
		});
	}

	_resetActions(ev) {
		ev?.preventDefault();
		ev?.stopPropagation();

		const actionUpdate = {
			'data.overworld.actions.act1': false,
			'data.overworld.actions.act2': false,
			'data.overworld.actions.act3': false,
			'data.overworld.actions.camp': false,
		};

		this.document.update(actionUpdate);
	}

	/**
	 * @param {Event} ev
	 */
	_doCamping(ev, { useAction = false } = {}) {
		// ev?.preventDefault();
		// ev?.stopPropagation();

		const systemData = this.document.data.data.overworld;
		if (systemData.actions.camp === false) {
			new Roll('1d20').evaluate({ async: true }).then(r => r.toMessage({ flavor: 'Camping', speaker: { actor: this.document } }));
			if (useAction) this.document.update({ 'data.overworld.actions.camp': true });
		}
	}

	activateListeners(jq) {
		super.activateListeners(jq);
		const html = jq[0];

		html.querySelectorAll('.rollable').forEach(el => el.addEventListener('click', this._roll.bind(this)));
		html.querySelector('.camp')?.addEventListener('click', this._doCamping.bind(this));

		html.querySelector('button.reset')?.addEventListener('click', this._resetActions.bind(this));
	}

	async _updateObject(event, formData) {
		const name = formData['name'];

		// Update prototype
		let nameChanged = false;
		if (name !== this.document.data.token.name) {
			formData['token.name'] = name;
			nameChanged = true;
		}

		const systemData = this.document.data.data.overworld ?? {};
		systemData.wounded = 0;
		const diff = formData['data.overworld.wounded'] - systemData.wounded;
		if (diff != 0) {
			const chatData = {
				speaker: { actor: this.document },
			};
			if (formData['data.overworld.wounded'] >= systemData.size) {
				chatData.content = '<i class="fas fa-skull"></i> Squad wiped out';
			}
			else {
				const value = Math.abs(diff);
				chatData.content = diff < 0 ? `<i class="fas fa-briefcase-medical"></i> <b>+${value}</b> Healed` : `<i class="fas fa-heart-broken"></i> <b>+${value}</b> Wounded`;
			}
			ChatMessage.applyRollMode(chatData, game.settings.get('core', 'rollMode'));
			ChatMessage.create(chatData);
		}

		await super._updateObject(event, formData);

		// Update placed tokens
		if (nameChanged) {
			this.document.getActiveTokens()
				.forEach(t => {
					if (t.name !== name) t.document.update({ name });
				});
		}
	}
}
