import { CFG } from '../config.mjs';

export class AgentSheet extends ActorSheet {
	get template() {
		return `modules/${CFG.module}/template/actor/agent.hbs`;
	}

	static get defaultOptions() {
		const _default = super.defaultOptions;
		return {
			..._default,
			classes: ['sheet', 'overworld-manager', 'actor', 'agent'],
			width: 'auto',
			height: 'auto',
			resizable: false,
		};
	}

	getData() {
		const _default = super.getData();

		const systemData = this.document.data.data.overworld ?? {
			actions: {
				act1: false,
				act2: false,
				act3: false,
				camp: false,
			},
			skills: {
				find: {
					base: 0,
				},
				fight: {
					base: 0,
				},
				talk: {
					base: 0,
				},
				cast: {
					base: 0,
				},
				move: {
					base: 0,
				}
			},
			baseDC: 0,
			size: 0,
		};

		['find', 'fight', 'talk', 'cast', 'move'].forEach(skId => {
			const sk = systemData.skills[skId] ?? {};
			sk.total = (sk.base ?? 0) + (sk.eq ?? 0) + (sk.res ?? 0) + (sk.trk ?? 0);
			systemData.skills[skId] = sk;
		});

		return {
			..._default,
			actor: this.document,
			data: systemData,
			skills: systemData.skills,
			actions: systemData.actions,
			campReady: systemData.actions.act1 && systemData.actions.act2 && systemData.actions.act3 && !systemData.actions.camp,
			notes: systemData.notes,
		};
	}

	_resetActions(ev) {
		ev?.preventDefault();
		ev?.stopPropagation();

		const actionUpdate = {
			'data.overworld.actions.act1': false,
			'data.overworld.actions.act2': false,
			'data.overworld.actions.act3': false,
			'data.overworld.actions.camp': false,
		};

		this.document.update(actionUpdate);
	}

	/**
	 * @param {Event} ev
	 */
	async _roll(ev) {
		ev?.preventDefault();
		ev?.stopPropagation();

		const el = ev.currentTarget;
		const skill = el.dataset.skill;
		const systemData = this.document.data.data.overworld;

		// Initialize
		const skData = systemData.skills[skill] ?? {};
		['total', 'base', 'eq', 'res']
			.forEach(key => {
				if (!Number.isSafeInteger(skData[key])) skData[key] = 0;
			});

		// Consume actions
		const act = systemData.actions;
		let usedAction = 0;
		if (!ev.shiftKey) {
			if (act.act1 !== true) {
				this.document.update({ 'data.overworld.actions.act1': true });
				usedAction = 1;
			}
			else if (act.act2 !== true) {
				this.document.update({ 'data.overworld.actions.act2': true });
				usedAction = 2;
			}
			else if (act.act3 !== true) {
				this.document.update({ 'data.overworld.actions.act3': true });
				usedAction = 3;
			}
		}
		else
			usedAction = -1;

		const actionMsg = usedAction < 0 ? ' (Reaction)' : usedAction == 0 ? ' (No Actions Left)' : ` (Action ${usedAction})`;

		// Build a roll
		const parts = ['1d20'];
		if (skData.base != 0) parts.push(`${skData.base}[Skill]`);
		if (skData.eq != 0) parts.push(`${skData.eq}[Equipment]`);
		if (skData.res != 0) parts.push(`${skData.res}[Research]`);
		if (skData.trk != 0) parts.push(`${skData.trk}[Trinkets]`);

		const roll = await new Roll(parts.join(' + ')).evaluate({ async: true });

		roll.toMessage({
			speaker: { actor: this.document },
			flavor: game.i18n.localize(`Overworld.Skill.${skill}`) + actionMsg,
		});
	}

	_healOnce(ev) {
		const systemData = this.document.data.data.overworld;
		let msg;
		if (systemData.critical === true) {
			this.document.update({ 'data.overworld.critical': false });
			msg = '<b>Weakened</b> condition';
		}
		else if (systemData.weakened === true) {
			this.document.update({ 'data.overworld.weakened': false });
			msg = '<b>Healthy</b> condition';
		}

		if (msg) {
			const chatData = {
				speaker: { actor: this.document },
				content: msg,
			};
			ChatMessage.applyRollMode(chatData, game.settings.get('core', 'rollMode'));
			ChatMessage.create(chatData);
		}
	}

	/**
	 * @param {Event} ev
	 */
	_doCamping(ev, { useAction = false } = {}) {
		// ev?.preventDefault();
		// ev?.stopPropagation();

		const systemData = this.document.data.data.overworld;
		if (systemData.actions.camp === false) {
			new Roll('1d20').evaluate({ async: true }).then(r => r.toMessage({ flavor: 'Camping', speaker: { actor: this.document } }));
			if (useAction) this.document.update({ 'data.overworld.actions.camp': true });
		}
	}

	/**
	 * @param {JQuery} jq
	 */
	activateListeners(jq) {
		super.activateListeners(jq);
		const html = jq[0];

		html.querySelector('button.reset')?.addEventListener('click', this._resetActions.bind(this));
		html.querySelector('button.heal')?.addEventListener('click', this._healOnce.bind(this));

		html.querySelector('.camp')?.addEventListener('click', this._doCamping.bind(this));

		html.querySelectorAll('.rollable').forEach(el => el.addEventListener('click', this._roll.bind(this)));

		html.querySelector('input[name="data.overworld.weakened"]')?.addEventListener('click', ev => {
			if (this.document.data.data.overworld.critical === true) {
				ev?.preventDefault();
				ev?.stopPropagation();
			}
		});
	}

	async _updateObject(event, formData) {
		const name = formData['name'];

		// Update prototype
		let nameChanged = false;
		if (name !== this.document.data.token.name) {
			formData['token.name'] = name;
			nameChanged = true;
		}

		if (formData['data.overworld.critical'] === true)
			formData['data.overworld.weakened'] = true;

		const systemData = this.document.data.data.overworld ?? {};
		systemData.critical ??= false;
		systemData.weakened ??= false;
		let msg;
		if (formData['data.overworld.critical'] === true && formData['data.overworld.critical'] !== systemData.critical)
			msg = '<b>Critical</b> condition';
		else if (formData['data.overworld.weakened'] === true && formData['data.overworld.weakened'] !== systemData.weakened || formData['data.overworld.weakened'] === undefined && formData['data.overworld.critical'] === false)
			msg = '<b>Weakened</b> condition';
		else if (formData['data.overworld.weakened'] === false && formData['data.overworld.weakened'] !== systemData.weakened)
			msg = '<b>Healthy</b> condition';

		if (msg) {
			const chatData = {
				speaker: { actor: this.document },
				content: msg,
			};
			ChatMessage.applyRollMode(chatData, game.settings.get('core', 'rollMode'));
			ChatMessage.create(chatData);
		}

		await super._updateObject(event, formData);

		// Update placed tokens
		if (nameChanged) {
			this.document.getActiveTokens()
				.forEach(t => {
					if (t.name !== name) t.document.update({ name });
				});
		}
	}
}
