import { CFG } from './config.mjs';

export class OverworldOverview extends FormApplication {
	get template() {
		return `modules/${CFG.module}/template/monitor.hbs`;
	}

	static get defaultOptions() {
		const _default = super.defaultOptions;
		return {
			..._default,
			title: 'Overworld Overview',
			id: 'overworld-overview',
			height: 'auto',
			width: 'auto',
			classes: [..._default.classes, 'overworld-manager', 'actor-overview'],
			dragDrop: [{ dragSelector: null, dropSelector: null }],
			submitOnChange: true,
			closeOnSubmit: false,
			submitOnClose: false,
		};
	}

	async getData() {
		const _default = super.getData();

		const actorCfg = deepClone(game.settings.get(CFG.module, 'actors') ?? {});

		const agents = [], squads = [], monsters = [], others = [];

		const isGM = game.user.isGM;

		function fillSystemInfo(a) {
			const sysData = a.actor.data.data.overworld;
			a.actions = sysData.actions;
			a.type = sysData.type;

			a.isAgent = a.type === 'agent';
			a.isMonster = a.type === 'monster';
			a.isSquad = a.type === 'squad';
			a.uuid = a.actor.uuid;

			if (!a.actor.testUserPermission(game.user, 'OWNER'))
				return;

			if (a.isAgent) agents.push(a);
			else if (a.isSquad) squads.push(a);
			else if (a.isMonster) monsters.push(a);
			else others.push(a);
		}

		actorCfg.actors?.forEach(a => {
			a.actor = game.actors.get(a.id);
			fillSystemInfo(a);
		});

		for (const a of actorCfg.tokens ?? []) {
			a.token = await fromUuid(a.uuid);
			a.actor = a.token.actor;
			fillSystemInfo(a);
		}

		agents.sort((a, b) => a.sort - b.sort);
		squads.sort((a, b) => a.sort - b.sort);
		monsters.sort((a, b) => a.sort - b.sort);

		this.agents = agents;
		this.squads = squads;
		this.monsters = monsters;
		this.others = others;
		this.all = [...agents, ...squads, ...monsters, ...others];

		return {
			..._default,
			actors: [
				{ label: 'Agents', list: agents },
				{ label: 'Squads', list: squads },
				{ label: 'Monsters', list: monsters },
				{ label: 'Unknown', list: others },
			],
			isEditable: game.user.isGM,
		};
	}

	_addActor(actorId) {
		const actor = game.actors.get(actorId);
		const isLinked = actor.data.token.actorLink;
		if (!isLinked) return ui.notifications.warn(`${actor.name} is not linked; add the unlinked tokens through add add token function.`);

		const sysData = actor.data.data.overworld;
		if (!['agent', 'squad', 'monster'].includes(sysData?.type))
			return ui.notifications.warn(`${actor.name} lacks proper data; ignored.`);

		const actorCfg = deepClone(game.settings.get(CFG.module, 'actors') ?? { actors: [] });

		actorCfg.actors ??= [];

		if (!actorCfg.actors.includes(actorId)) {
			actorCfg.actors.push({ id: actorId, sort: 1000 });
			game.settings.set(CFG.module, 'actors', actorCfg);
		}
		else {
			return ui.notifications.warn(`${actor.name} is already listed.`);
		}
	}

	async _onDrop(event) {
		let data;
		try {
			data = JSON.parse(event.dataTransfer.getData('text/plain'));
		}
		catch (err) {
			console.error(err);
			return false;
		}

		if (data.type === 'Actor')
			this._addActor(data.id);
	}

	_deleteActor(uuid) {
		const doc = this.all?.find(a => a.uuid == uuid);
		const actor = doc?.actor ?? doc;

		const actorCfg = deepClone(game.settings.get(CFG.module, 'actors') ?? {});
		actorCfg.actors ??= [];
		actorCfg.tokens ??= [];

		Dialog.confirm({
			title: 'Remove Actor?',
			content: `Remove "${actor.token?.name ?? actor.name}"?`,
			yes: () => {
				if (actor.isToken)
					actorCfg.tokens = actorCfg.tokens.filter(t => t.uuid != uuid);
				else
					actorCfg.actors = actorCfg.actors.filter(a => a.id != actor.id);
				game.settings.set(CFG.module, 'actors', actorCfg);
			},
			defaultYes: true,
		});
	}

	/**
	 * @param {Event} ev
	 */
	async _resetAll(ev) {
		ev.preventDefault();
		ev.stopPropagation();

		console.log('Reset All');

		for (const d of this.all) {
			const actor = d.actor;
			const type = actor.data.data.overworld?.type;
			const updateData = {};
			switch (type) {
				case 'monster':
					updateData['data.overworld.actions.attack'] = false;
					updateData['data.overworld.actions.move'] = false;
					break;
				case 'agent':
				case 'squad':
					updateData['data.overworld.actions.act1'] = false;
					updateData['data.overworld.actions.act2'] = false;
					updateData['data.overworld.actions.act3'] = false;
					updateData['data.overworld.actions.camp'] = false;
					break;
			}
			await actor.update(updateData);
		}

		ui.notifications.warn('Reset complete!');
		const chatData = {
			content: 'All actors reset.'
		};
		ChatMessage.applyRollMode(chatData, game.settings.get('core', 'rollMode'));
		ChatMessage.create(chatData);
	}

	/**
	 * @param {Event} ev
	 */
	async _addUnlinked(ev) {
		ev.preventDefault();
		ev.stopPropagation();

		const templatePath = `modules/${CFG.module}/template/token-selector.hbs`;
		await loadTemplates([templatePath]);

		const uuids = this.all?.map(t => t.uuid);

		const tokens = canvas.tokens.placeables
			.map(t => t.document)
			.filter(t => t.actor && !t.isLinked)
			.map(t => t.actor)
			.filter(a => a.data.data.overworld?.type !== undefined)
			.filter(a => !uuids.includes(a.uuid));

		if (tokens.length === 0) {
			ui.notifications.warn('No valid tokens found');
			return;
		}

		const templateData = {
			tokens,
		};
		const content = await renderTemplate(templatePath, templateData);

		const uuid = await new Promise(resolve => new Dialog({
			content,
			buttons: {
				add: {
					label: 'Add Token',
					callback: jq => {
						const html = jq[0];
						const formData = new FormDataExtended(html.querySelector('form')).toObject()
						resolve(formData.token);
					}
				}
			},
			close: _ => resolve(null),
			jQuery: false,
		}).render(true, { focus: true })
		);

		if (uuid) {
			const actorCfg = deepClone(game.settings.get(CFG.module, 'actors') ?? { });

			actorCfg.tokens ??= [];

			const uuids = actorCfg.tokens.map(t => t.uuid);

			if (!uuids.includes(uuid)) {
				actorCfg.tokens.push({ uuid, sort: 1000 });
				game.settings.set(CFG.module, 'actors', actorCfg);
			}
		}
	}

	/**
	 * @param {Event} ev
	 */
	_openSheet(ev) {
		ev.preventDefault();
		ev.stopPropagation();

		const uuid = ev.currentTarget.dataset.uuid;

		fromUuid(uuid).then(doc => {
			const actor = doc?.actor ?? doc;
			actor?.sheet.render(true, { focus: true })
		});
	}

	/**
	 * @param {JQuery} jq
	 */
	activateListeners(jq) {
		super.activateListeners(jq);
		const html = jq[0];
		html.querySelectorAll('.control-icon.delete')
			.forEach(el => {
				el.addEventListener('click', ev => {
					ev.preventDefault();
					ev.stopPropagation();

					const li = ev.currentTarget.closest('[data-uuid]');
					this._deleteActor(li.dataset.uuid);
				});
			});

		html.querySelectorAll('li[data-uuid]').forEach(el => el.addEventListener('contextmenu', this._openSheet.bind(this)));

		html.querySelector('button.reset-all')?.addEventListener('click', this._resetAll.bind(this));
		html.querySelector('button.add-unlinked')?.addEventListener('click', this._addUnlinked.bind(this));
	}

	_updateObject(event, formData) {

	}
}
