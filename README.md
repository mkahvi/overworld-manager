# Overworld Manager

Overworld manager for certain kinds of games in PF1 on Foundry VTT.

Adds extra specialized sheets and a general overview dialog.

### Agent

Most detailed actor, representing specialists.

- 5 skills with several bonus types.
- Two health levels.
- Three generic actions plus special camping action.

### Squad

- Single skill keyed off from squad size (strength).
- Health is the number of wounded in the squad. Once all are wounded, the squad is considered wiped out.
- Action economy similar to that of an agent.

### Monster

- Size (Strength)
- Base DC (10 + Strength)
- Attack & Move actions instead of agent-like three + camp.
- Health is tracked simply as the size/strength of the monster.

## Install

Manifest URL: <https://gitlab.com/mkahvi/overworld-manager/-/raw/latest/module.json>

## Attribution

If you use any of the code in this project, I would appreciate I or the project was credited for inspiration or whatever where appropriate. Or just drop a line about using my code. I do not mind not having this, but it's just nice knowing something has come out of my efforts.

## Donations

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/I2I13O9VZ)

## License

This software is distributed under the [MIT License](./LICENSE).
